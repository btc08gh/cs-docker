# Docker container for a simple custom nsp/xci shop
## Usage
```
docker-compose up --build
```
Once the build is complete press ctrl-c once to gracefully stop the container

Now you can use the following to start the container.  Start is much quicker than build.
```
docker start custom-shop
```
shop runs on port 7000 by default (you can change it in the docker-compose.yml)

to add to tinfoil:
```
Protocol: http
Host: your.ip.address
Port: 7000
Path: /tinfoil.php
title: custom shop
```
