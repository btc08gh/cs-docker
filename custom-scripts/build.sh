#!/bin/bash
git config --global --add safe.directory '*'
git config --global init.defaultBranch main

apk add composer

cp /custom-files/php-www2.conf /config/php/www2.conf

cd /config/www
rm -f index.html
git init
git pull https://gitlab.com/izenn/custom-shop.git
composer require flowjs/flow-php-server
wget -O flat.png https://github.com/halgatewood/file-directory-list/raw/master/flat.png

chown -R abc:abc /config/www
